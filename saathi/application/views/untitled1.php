CASE WHEN registeredOn = '0000-00-00' THEN NULL ELSE DATE_FORMAT(registeredOn,'%d/%m/%Y') END registeredOn
CASE WHEN dob = '0000-00-00' THEN NULL ELSE DATE_FORMAT(`dob`,'%d/%m/%Y') END dob
CASE WHEN fingerDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(fingerDate,'%d/%m/%Y') END fingerDate
CASE WHEN saictcDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(saictcDate,'%d/%m/%Y') END saictcDate
CASE WHEN hivDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(hivDate,'%d/%m/%Y') END hivDate
CASE WHEN reportIssuedDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(reportIssuedDate,'%d/%m/%Y') END reportIssuedDate
CASE WHEN artDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(artDate,'%d/%m/%Y') END artDate


public function userById($id = NULL) {				
		$sql="SELECT `userId`,`userType`,`client_id`,`userUniqueId`,`userName`,`roleId`,`assignedId`,`password`,`name`,`nameAlias`,`domainOfWork`,`monthlyIncome`,`noOfChildren`,`male_children`,`female_children`,`total_children`,`referralPoint`,`referralPoint_others`,`addressState`,`addressDistrict`,`address`,`primaryIdentity`,`primaryIdentity_others`,`secondaryIdentity`,`secondaryIdentity_other`,`hivHistory`,`gender`,`emailAddress`,`age`,CASE WHEN dob = '0000-00-00' THEN NULL ELSE DATE_FORMAT(`dob`,'%d/%m/%Y') END dob,`occupation`,`occupation_other`,`educationalLevel`,`districtId`,`state`,`placeOforigin`,TRIM(LEADING '+91' FROM `mobileNo`) AS mobileNo,`maritalStatus`,`maritalStatus_other`,`sought`,`condomUsage`,`substanceUse`,`multipleSexPartner`,`prefferedSexualAct`,`pastHivReport`,`testHiv`,`prefferedGender`,`hivTestTime`,`hivTestResult`,CASE WHEN fingerDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(fingerDate,'%d/%m/%Y') END fingerDate,`fingerReport`,`saictcStatus`,CASE WHEN saictcDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(saictcDate,'%d/%m/%Y') END saictcDate,`saictcPlace`,`ictcNumber`,CASE WHEN hivDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(hivDate,'%d/%m/%Y') END hivDate,`hivStatus`,CASE WHEN reportIssuedDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(reportIssuedDate,'%d/%m/%Y') END reportIssuedDate,`reportStatus`,`campCode`,`artCenter`,`artNumber`,`cd4status`,`cd4Result`,`artStatus`,`syphilisTest`,`syphilisResult`,`tb_test`,`tbResult`,`rntcpRefer`,`remark`,`shareInfoAboutSexualBehaviour`,`sexualBehaviour`,`hydc`,`registerFromDevice`,`registerMode`,`smsUser`,`registeredBy`,CASE WHEN registeredOn = '0000-00-00' THEN NULL ELSE DATE_FORMAT(registeredOn,'%d/%m/%Y') END registeredOn,`registrationNumber`,`modeOfContact`,`hrg`,`arg`,`ictcUpload`,`linkToArt`,CASE WHEN artDate = '0000-00-00' THEN NULL ELSE DATE_FORMAT(artDate,'%d/%m/%Y') END artDate,`artUpload`,`otherService`,`clientStatus`,`referralSlip`,`ictcReportScan`,`createdBy`,`createdDate`,`updatedDate`,`updatedBy`,`otp`,`userVerify`,`deleted`,`agreeSms` 

		 FROM `tbl_user` WHERE  userVerify = 'Y' and userId = ".$id."";
		$query = $this->db->query($sql);
		//echo $this->db->last_query(); exit;	
		$res = $query->result_array();		
		return $res;
    }