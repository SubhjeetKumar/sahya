	<div class="clear"></div>
</div><!-- .container /-->
<?php tie_banner('banner_bottom' , '<div class="e3lan e3lan-bottom">' , '</div>' ); ?>

<?php get_sidebar( 'footer' ); ?>				
<div class="clear"></div>
<div class="footer-bottom">
	<div class="container">
		<!-- <div class="alignright"> -->
		<div class="footer-menu">
			<?php
				$footer_vars = array('%year%' , '%site%' , '%url%');
				$footer_val  = array( date('Y') , get_bloginfo('name') , home_url() );
				$footer_two  = str_replace( $footer_vars , $footer_val , tie_get_option( 'footer_two' ));
				echo htmlspecialchars_decode( $footer_two );?>
		</div>
		<?php if( tie_get_option('footer_social') ) tie_get_social( true , false, 'ttip-none' ); ?>
		
		<!-- <div class="alignleft"> -->
		<div class="copyright">
			<?php
				$footer_one  = str_replace( $footer_vars , $footer_val , tie_get_option( 'footer_one' ));
				echo htmlspecialchars_decode( $footer_one );?>
		</div>
		<div class="clear"></div>
	</div><!-- .Container -->
</div><!-- .Footer bottom -->

</div><!-- .inner-Wrapper -->
</div><!-- #Wrapper -->
</div><!-- .Wrapper-outer -->
<?php if( tie_get_option('footer_top') ): ?>
	<div id="topcontrol" class="fa fa-angle-up" title="<?php _eti( 'Scroll To Top' ); ?>"></div>
<?php endif; ?>
<div id="fb-root"></div>
<?php wp_footer();?>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
var jN = jQuery.noConflict(true);
jN(window).scroll(function(){
    if (jN(window).scrollTop() >= 173) {
       jN('#theme-header').addClass('fixed-header');       
    }
    else {
       jN('#theme-header').removeClass('fixed-header');
    }
});
</script>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
var jNw = jQuery.noConflict(true);
jNw(window).load(function(){	
	jNw('.page-id-731 #wrapper').removeClass('boxed-all');
	jNw('.page-id-731 #wrapper').addClass('wide-layout');
	
	jNw('.appstore, .playstore').click(function(){
		//alert('Coming Soon');
		jNw('#modal').show();
	});

	if(sessionStorage.getItem('popState') != 'shown'){

		jNw("#ac-wrapper").show();
		sessionStorage.setItem('popState','shown');
	}
<?php if($_SESSION['isLogin'] == 'yes'){ ?>

		var logedUser	 = '<?php echo $_SESSION['user']; ?>';
		var logedUserName	 = '<?php echo $_SESSION['user']; ?>';
		var logedUserMobile	 = '<?php echo $_SESSION['loginmobile']; ?>';
		var logedUserEmail	 = '<?php echo $_SESSION['loginemail']; ?>';
		jNw("#menu-item-1092").hide();
		jNw("#menu-item-702").hide();
		jNw("#menu-item-1817 a").text(logedUser	);
		jNw("#author").attr('required','false');
		jNw("#phone").attr('required','false');
		jNw("#email").attr('required','false');
		jNw("#author").val(logedUserName);
		jNw("#phone").val(logedUserMobile);
		jNw("#email").val(logedUserEmail);
		jNw(".comment-form-author").hide();
		jNw(".comment-form-email").hide();
		jNw(".comment-form-url").hide();
		jNw(".comment-form-cookies-consent").hide();	
		jNw(".comment-form-phone").hide();		


<?php }else{ ?> 
	jNw("#menu-item-1702,.menu-item-1702").css('display','none');	
	jNw("#menu-item-1758").hide();
	jNw("#menu-item-1871,.menu-item-1871").hide();
	jNw("#menu-item-1817").hide();
	
<?php } ?>	


	});
</script>

<script type="text/javascript">
function PopUp(){
        document.getElementById('ac-wrapper').style.display="none"; 
}
function CloseModal(){
    var dd = document.getElementById('modal');
	dd.style.display = "none"; 
}
function close_window(){
     //alert("close");
     //  window.close();
	 sessionStorage.setItem('popState','');
     window.open('https://www.google.com/','_parent',''); 
     window.close(); 
  
}
</script>


</body>
</html>